# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='FacebookLike',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('user_id', models.IntegerField()),
                ('facebook_id', models.BigIntegerField()),
                ('name', models.TextField(blank=True, null=True)),
                ('category', models.TextField(blank=True, null=True)),
                ('created_time', models.DateTimeField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='FacebookProfile',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('about_me', models.TextField(blank=True, null=True)),
                ('facebook_id', models.BigIntegerField(blank=True, unique=True, null=True)),
                ('access_token', models.TextField(help_text='Facebook token for offline access', blank=True, null=True)),
                ('facebook_name', models.CharField(blank=True, max_length=255, null=True)),
                ('facebook_profile_url', models.TextField(blank=True, null=True)),
                ('website_url', models.TextField(blank=True, null=True)),
                ('blog_url', models.TextField(blank=True, null=True)),
                ('date_of_birth', models.DateField(blank=True, null=True)),
                ('gender', models.CharField(blank=True, choices=[('m', 'Male'), ('f', 'Female')], max_length=1, null=True)),
                ('raw_data', models.TextField(blank=True, null=True)),
                ('facebook_open_graph', models.NullBooleanField(help_text='Determines if this user want to share via open graph')),
                ('new_token_required', models.BooleanField(help_text='Set to true if the access token is outdated or lacks permissions', default=False)),
                ('image', models.ImageField(blank=True, upload_to='images/facebook_profiles/%Y/%m/%d', max_length=255, null=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='FacebookUser',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('user_id', models.IntegerField()),
                ('facebook_id', models.BigIntegerField()),
                ('name', models.TextField(blank=True, null=True)),
                ('gender', models.CharField(blank=True, choices=[('F', 'female'), ('M', 'male')], max_length=1, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='OpenGraphShare',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('action_domain', models.CharField(max_length=255)),
                ('facebook_user_id', models.BigIntegerField()),
                ('share_dict', models.TextField(blank=True, null=True)),
                ('object_id', models.PositiveIntegerField(blank=True, null=True)),
                ('error_message', models.TextField(blank=True, null=True)),
                ('last_attempt', models.DateTimeField(auto_now_add=True, null=True)),
                ('retry_count', models.IntegerField(blank=True, null=True)),
                ('share_id', models.CharField(blank=True, max_length=255, null=True)),
                ('completed_at', models.DateTimeField(blank=True, null=True)),
                ('removed_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_at', models.DateTimeField(db_index=True, auto_now_add=True)),
                ('content_type', models.ForeignKey(null=True, blank=True, to='contenttypes.ContentType')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'django_facebook_open_graph_share',
            },
        ),
        migrations.AlterUniqueTogether(
            name='facebookuser',
            unique_together=set([('user_id', 'facebook_id')]),
        ),
        migrations.AlterUniqueTogether(
            name='facebooklike',
            unique_together=set([('user_id', 'facebook_id')]),
        ),
    ]
